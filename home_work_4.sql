-- создание базы данных shop
CREATE SCHEMA `shop` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;

-- создание таблицы "категория товаров"
CREATE TABLE `shop`.`category` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(128) NOT NULL,
  `discount` TINYINT NOT NULL,
  `alias_name` VARCHAR(128) NULL,
PRIMARY KEY (`id`));

-- создание таблицы "бренд"
CREATE TABLE `shop`.`brand` (
  `id` INT NOT NULL,
  `name` VARCHAR(128) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`));

-- создание таблицы "тип товара"
CREATE TABLE `shop`.`product_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(128) NOT NULL,
PRIMARY KEY (`id`));

